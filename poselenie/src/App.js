import React, { Component } from 'react';
import './App.css';
import { Container, Row, Col } from 'reactstrap';

// Components
import MapContainer from './components/GoogleMap';
import Typing from 'react-typing-animation';


class App extends Component {
  render() {
    return (
      <div className="App" >
      <Container>
        <Row>
          <Col>
          <header className="App-header">
            <img src='https://cdn0.iconfinder.com/data/icons/website-design-4/468/World_map_with_pointer_icon-512.png' className="App-logo" alt="logo" />
              Take care of a settlement. 
            <Typing >
              <div>
                <Typing.Delay ms={1000} /> Be a HERO they need!
              </div>
            </Typing>



          </header>
          </Col>
          <Col>
            <MapContainer />
          </Col>
        </Row>
      </Container>   

      </div>
    );
  }
}

export default App;
